import { UserInterface } from './user.interface';

export class User implements UserInterface {
    firstName: string;
    lastName: string;
    id: number;

    constructor(userOptions: UserInterface) {
        this.id = userOptions.id;
        this.firstName = userOptions.firstName;
        this.lastName = userOptions.lastName;
    }
}
