export const defaultCourse = {
    id: 0,
    title: '',
    description: '',
    creationDate: new Date(0),
    duration: 0,
    imageUrl: ''
};

export const coursesData = [
    {
        id: 7,
        title: 'Module #7: Routing',
        description: `It is a long established fact that a reader will be distracted by t
        he readable content of a page when looking at its layout.
        The point of using Lorem Ipsum is that it has a more-or-less normal
        distribution of letters, as opposed to using 'Content here, content here',
        making it look like readable English. Many desktop publishing packages and web
        page editors now use Lorem`,
        creationDate: new Date('Jan 28, 2019'),
        duration: 120,
        imageUrl: 'assets/img/routing.png'
    },
    {
        id: 8,
        title: 'Module #8: HTTP',
        description: `It is a long established fact that a reader will be distracted by t
        he readable content of a page when looking at its layout.
        The point of using Lorem Ipsum is that it has a more-or-less normal
        distribution of letters, as opposed to using 'Content here, content here',
        making it look like readable English. Many desktop publishing packages and web
        page editors now use Lorem`,
        creationDate: new Date('Feb 04, 2019'),
        duration: 180,
        imageUrl: 'assets/img/http.jpg'
    },
    {
        id: 9,
        title: 'Module #9: Rxjs observables',
        description: `It is a long established fact that a reader will be distracted by t
        he readable content of a page when looking at its layout.
        The point of using Lorem Ipsum is that it has a more-or-less normal
        distribution of letters, as opposed to using 'Content here, content here',
        making it look like readable English. Many desktop publishing packages and web
        page editors now use Lorem`,
        creationDate: new Date('Feb 11, 2019'),
        duration: 150,
        imageUrl: 'assets/img/rxjs.png'
    },
    {
        id: 10,
        title: 'Module #10: Ngrx/Redux',
        description: `It is a long established fact that a reader will be distracted by t
        he readable content of a page when looking at its layout.
        The point of using Lorem Ipsum is that it has a more-or-less normal
        distribution of letters, as opposed to using 'Content here, content here',
        making it look like readable English. Many desktop publishing packages and web
        page editors now use Lorem`,
        creationDate: new Date('Feb 19, 2019'),
        duration: 120,
        imageUrl: 'assets/img/ngrx.png'
    },
    {
        id: 11,
        title: 'Module #11: Forms',
        description: `It is a long established fact that a reader will be distracted by t
        he readable content of a page when looking at its layout.
        The point of using Lorem Ipsum is that it has a more-or-less normal
        distribution of letters, as opposed to using 'Content here, content here',
        making it look like readable English. Many desktop publishing packages and web
        page editors now use Lorem`,
        creationDate: new Date('Feb 25, 2019'),
        duration: 120,
        imageUrl: 'assets/img/forms.png'
    },
    {
        id: 12,
        title: 'Module #12: Internationalization',
        description: `It is a long established fact that a reader will be distracted by t
        he readable content of a page when looking at its layout.
        The point of using Lorem Ipsum is that it has a more-or-less normal
        distribution of letters, as opposed to using 'Content here, content here',
        making it look like readable English. Many desktop publishing packages and web
        page editors now use Lorem`,
        creationDate: new Date('Mar 04, 2019'),
        duration: 45,
        imageUrl: 'assets/img/internationalization.png'
    },
    {
        id: 1,
        title: 'Module #1: Webpack/Typescript/Angular Intro',
        description: `It is a long established fact that a reader will be distracted by t
        he readable content of a page when looking at its layout.
        The point of using Lorem Ipsum is that it has a more-or-less normal
        distribution of letters, as opposed to using 'Content here, content here',
        making it look like readable English. Many desktop publishing packages and web
        page editors now use Lorem`,
        creationDate: new Date('Nov 19, 2018'),
        duration: 120,
        imageUrl: 'assets/img/webpack.png'
    },
    {
        id: 2,
        title: 'Module #2: Components',
        description: `It is a long established fact that a reader will be distracted by t
        he readable content of a page when looking at its layout.
        The point of using Lorem Ipsum is that it has a more-or-less normal
        distribution of letters, as opposed to using 'Content here, content here',
        making it look like readable English. Many desktop publishing packages and web
        page editors now use Lorem`,
        creationDate: new Date('Dec 03, 2018'),
        duration: 115,
        imageUrl: 'assets/img/components.png'
    },
    {
        id: 3,
        title: 'Module #3: Unit testing',
        description: `It is a long established fact that a reader will be distracted by t
        he readable content of a page when looking at its layout.
        The point of using Lorem Ipsum is that it has a more-or-less normal
        distribution of letters, as opposed to using 'Content here, content here',
        making it look like readable English. Many desktop publishing packages and web
        page editors now use Lorem`,
        creationDate: new Date('Dec 10, 2018'),
        duration: 90,
        imageUrl: 'assets/img/unit-testing.jpg'
    },
    {
        id: 4,
        title: 'Module #4: Directives + Pipes',
        description: `It is a long established fact that a reader will be distracted by t
        he readable content of a page when looking at its layout.
        The point of using Lorem Ipsum is that it has a more-or-less normal
        distribution of letters, as opposed to using 'Content here, content here',
        making it look like readable English. Many desktop publishing packages and web
        page editors now use Lorem`,
        creationDate: new Date('Dec 17, 2018'),
        duration: 85,
        imageUrl: 'assets/img/directive.png'
    },
    {
        id: 5,
        title: 'Module #5: Modules & Services',
        description: `It is a long established fact that a reader will be distracted by t
        he readable content of a page when looking at its layout.
        The point of using Lorem Ipsum is that it has a more-or-less normal
        distribution of letters, as opposed to using 'Content here, content here',
        making it look like readable English. Many desktop publishing packages and web
        page editors now use Lorem`,
        creationDate: new Date('Jan 14, 2019'),
        duration: 135,
        imageUrl: 'assets/img/services.png'
    },
    {
        id: 6,
        title: 'Module #6: Change detection',
        description: `It is a long established fact that a reader will be distracted by t
        he readable content of a page when looking at its layout.
        The point of using Lorem Ipsum is that it has a more-or-less normal
        distribution of letters, as opposed to using 'Content here, content here',
        making it look like readable English. Many desktop publishing packages and web
        page editors now use Lorem`,
        creationDate: new Date('Jan 21, 2019'),
        duration: 225,
        imageUrl: 'assets/img/change-detection.jpeg'
    }
];
