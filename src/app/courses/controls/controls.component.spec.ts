import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ControlsComponent } from './controls.component';

describe('ControlsComponent', () => {
    let component: ControlsComponent;
    let fixture: ComponentFixture<ControlsComponent>;
    let searchQuery: string;

    beforeEach(async(() => {
        spyOn(console, 'log');
        searchQuery = 'searchQuery';

        TestBed.configureTestingModule({
            imports: [FormsModule],
            declarations: [ControlsComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ControlsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should display a search query within an input element', fakeAsync(() => {
        component.searchQuery = searchQuery;
        fixture.detectChanges();
        const inputEl = fixture.nativeElement.querySelector('input');
        tick();

        expect(inputEl.value).toBe(searchQuery);
    }));

    it('should make search by search query', () => {
        component.searchQuery = searchQuery;
        fixture.detectChanges();
        const findButtonEl = fixture.nativeElement.querySelector('button[matSuffix]');
        findButtonEl.click();

        expect(console.log).toHaveBeenCalledWith(searchQuery);
    });
});
