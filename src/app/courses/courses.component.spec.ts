import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { Component, Input, Output, EventEmitter, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { CoursesComponent } from './courses.component';
import { CourseInterface } from './course.interface';
import { defaultCourse } from './courses-data/courses-data';

@Component({
    selector: 'app-course-item',
    template: `<div class="course">
        <h3 class="course__title">{{course.title}}</h3>
        <button (click)="deleteCourse.emit(course)" class="course__delete">Delete</button>
    </div>`
})
class FakeAppCourseItemComponent {
    @Input() course: CourseInterface = defaultCourse;
    @Output() deleteCourse = new EventEmitter<CourseInterface>();
}
describe('CoursesComponent', () => {
    let component: CoursesComponent;
    let fixture: ComponentFixture<CoursesComponent>;

    let course1: CourseInterface;
    let course2: CourseInterface;
    let fakeCourseData: CourseInterface[];
    let orderedFakeCourseData: CourseInterface[];

    beforeEach(async(() => {
        spyOn(console, 'log');

        course1 = {
            id: 1,
            title: 'title1',
            description: 'description1',
            creationDate: new Date('Feb 04, 2000'),
            duration: 180,
            imageUrl: 'imageUrl1'
        };

        course2 = {
            id: 2,
            title: 'title2',
            description: 'description2',
            creationDate: new Date('Jan 28, 2019'),
            duration: 200,
            imageUrl: 'imageUrl2'
        };

        fakeCourseData = [course2, course1];
        orderedFakeCourseData = [course1, course2];

        TestBed.configureTestingModule({
            declarations: [CoursesComponent, FakeAppCourseItemComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CoursesComponent);
        component = fixture.componentInstance;
        component.courses = fakeCourseData;

        fixture.detectChanges();
    });

    it('should sort courses data by date', () => {
        expect(component.courses).toEqual(orderedFakeCourseData);
    });

    it('should load more data', fakeAsync(() => {
        const loadMoreButton = fixture.nativeElement.querySelector('.courses__load-more');
        loadMoreButton.click();
        tick();
        fixture.detectChanges();

        expect(console.log).toHaveBeenCalledWith('Load More');
    }));

    describe('display courses', () => {
        let coursesElements: Element[];

        beforeEach(() => {
            coursesElements = fixture.nativeElement.querySelectorAll('.course__title');
        });

        it('should display courses', () => {
            expect(coursesElements.length).toBe(2);
        });

        it('should display first course', () => {
            expect(coursesElements[0].innerHTML).toBe(course1.title);
        });

        it('should display second course', () => {
            expect(coursesElements[1].innerHTML).toBe(course2.title);
        });
    });

    it('should delete course', fakeAsync(() => {
        const deleteButton = fixture.nativeElement.querySelector('.course__delete');
        deleteButton.click();
        tick();

        expect(console.log).toHaveBeenCalledWith(`Delete course #${course1.id}`);
    }));

    it('should show breadcrumbs', () => {
        expect(fixture.nativeElement.querySelector('app-breadcrumbs')).toBeTruthy();
    });

    it('should show control panel', () => {
        expect(fixture.nativeElement.querySelector('app-controls')).toBeTruthy();
    });
});
