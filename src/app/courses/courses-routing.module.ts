import { RouterModule, Routes } from '@angular/router';
import { CoursesComponent } from './courses.component';

const coursesRoutes: Routes = [
    {
        path: 'courses',
        children: [
            { path: '', component: CoursesComponent }
        ]
    }
];

export const CoursesRoutingModule = RouterModule.forChild(coursesRoutes);
