import { CourseInterface } from './course.interface';

export class Course implements CourseInterface {
    id: number;
    title: string;
    creationDate: Date;
    duration: number;
    description: string;

    constructor(courseOptions: CourseInterface) {
        this.id = courseOptions.id;
        this.title = courseOptions.title;
        this.creationDate = courseOptions.creationDate;
        this.duration = courseOptions.duration;
        this.description = courseOptions.description;
    }
}
