import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';

import { CoursesComponent } from './courses.component';
import { CourseItemComponent } from './course-item/course-item.component';
import { ControlsComponent } from './controls/controls.component';
import { SharedModule } from '@shared/shared.module';
import { CoursesRoutingModule } from './courses-routing.module';

@NgModule({
    declarations: [
        CoursesComponent,
        CourseItemComponent,
        ControlsComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        CoursesRoutingModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatIconModule,
        MatToolbarModule,
        SharedModule
    ]
})
export class CoursesModule { }
