import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';

import { CourseItemComponent } from './course-item.component';
import { CourseInterface } from '../course.interface';
import { defaultCourse } from '../courses-data/courses-data';

@Component({
    template: `
      <app-course-item
        [course]="course" (deleteCourse)="delete($event)">
      </app-course-item>`
})
class TestHostComponent {
    course: CourseInterface = defaultCourse;
    delete = jasmine.createSpy('deleteSpy');
}

describe('CourseItemComponent', () => {
    let component: TestHostComponent;
    let fixture: ComponentFixture<TestHostComponent>;
    let fakeCourse: CourseInterface;

    beforeEach(async(() => {
        fakeCourse = {
            id: 25,
            title: 'title',
            creationDate: new Date('Feb 04, 2000'),
            duration: 100,
            description: 'description',
            imageUrl: 'imageUrl'
        };

        TestBed.configureTestingModule({
            declarations: [CourseItemComponent, TestHostComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TestHostComponent);
        component = fixture.componentInstance;
        component.course = fakeCourse;
        fixture.detectChanges();
    });

    it('should calculate and display course duration', () => {
        const durationEl = fixture.nativeElement.querySelectorAll('.course__info-item')[0];

        expect(durationEl.innerHTML).toBe('1h:40min');
    });

    it('should   calculate and display duration which is less than one hour', () => {
        component.course = { ...fakeCourse, duration: 40 };
        fixture.detectChanges();
        const durationEl = fixture.nativeElement.querySelectorAll('.course__info-item')[0];

        expect(durationEl.innerHTML).toBe('40min');
    });

    it('should calculate and display date', () => {
        const dateEl = fixture.nativeElement.querySelectorAll('.course__info-item')[1];

        expect(dateEl.innerHTML).toBe('Feb 04, 2000');
    });

    it('should display course title', () => {
        const titleEl = fixture.nativeElement.querySelector('.course__title');

        expect(titleEl.innerHTML).toBe(fakeCourse.title);
    });

    it('should display description', () => {
        const descriptionEl = fixture.nativeElement.querySelector('.course__description');

        expect(descriptionEl.innerHTML).toBe(fakeCourse.description);
    });

    it('should delete course', () => {
        const deleteButtonEl = fixture.nativeElement.querySelectorAll('.course__button')[1];
        deleteButtonEl.click();

        expect(component.delete).toHaveBeenCalledWith(fakeCourse);
    });
});
