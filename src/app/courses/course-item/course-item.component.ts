import { ChangeDetectionStrategy, Component, Input, Output, EventEmitter } from '@angular/core';
import { CourseInterface } from '../course.interface';
import { defaultCourse } from '../courses-data/courses-data';

@Component({
    selector: 'app-course-item',
    templateUrl: './course-item.component.html',
    styleUrls: ['./course-item.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CourseItemComponent {
    @Input() course: CourseInterface = defaultCourse;
    @Output() deleteCourse = new EventEmitter<CourseInterface>();

    onDeleteCourse(): void {
        this.deleteCourse.emit(this.course);
    }

    get duration(): string {
        const hours = Math.floor(this.course.duration / 60);
        const minutes = this.course.duration - hours * 60;

        return `${hours ? hours : ''}${hours ? 'h:' : ''}${minutes}min`;
    }

    get date(): string {
        return this.course.creationDate.toLocaleDateString('en-US', {
            month: 'short',
            day: '2-digit',
            year: 'numeric'
        });
    }

}
