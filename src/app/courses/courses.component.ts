import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CourseInterface } from './course.interface';
import { coursesData } from './courses-data/courses-data';

@Component({
    selector: 'app-courses',
    templateUrl: './courses.component.html',
    styleUrls: ['./courses.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CoursesComponent implements OnInit {
    courses: CourseInterface[] = coursesData;

    constructor() { }

    ngOnInit() {
        this.courses = this.courses
            .sort((course1, course2) => course1.creationDate.getTime() - course2.creationDate.getTime());
    }

    onDeleteCourse(course: CourseInterface): void {
        console.log(`Delete course #${course.id}`);
    }

    onLoadMore(): void {
        console.log('Load More');
    }
}
