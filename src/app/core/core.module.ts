import { CommonModule } from '@angular/common';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';

import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { throwIfAlreadyLoaded } from './moduleImportGuard';
import { AuthService } from './auth/auth.service';

@NgModule({
    imports: [
        CommonModule,
        MatToolbarModule,
        MatButtonModule
    ],
    declarations: [
        HeaderComponent,
        FooterComponent,
        LoginComponent
    ],
    providers: [AuthService],
    exports: [
        HeaderComponent,
        FooterComponent,
        LoginComponent
    ]
})
export class CoreModule {
    /* make sure CoreModule is imported only by one NgModule the AppModule */
    constructor(
        @Optional() @SkipSelf() parentModule: CoreModule
    ) {
        throwIfAlreadyLoaded(parentModule, 'CoreModule');
    }
}
